const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

//routes
const invites = require("./routes/invites");
const backpack = require("./routes/backpack");
//models
require("./models/Backpack");
require("./models/Invites");

const app = express();
const PORT = process.env.PORT;

app.use(bodyParser.json({}));
app.use([invites, backpack]); //registering routes

mongoose
  .connect("mongodb://127.0.0.1/metaverse-game", () => {
    console.log("Connected To Mongo-DB");
    app.listen(PORT, () => {
      console.log(`Connected To Server At Port ${PORT}`);
    });
  })
  .catch((err) => {
    console.log(err);
  });