const express = require("express");
const router = express.Router();

//create
router.post("/post/backpack/:u_id"); //create a backpack for a user
//read
router.get("/get/backpack/:b_id"); //get single backpack of a user
router.get("/get/all-backpack"); //get all backpacks
router.get("/get/all-food-from-backpack/:b_id"); //gets all food from a backpack
router.get("/get/single-food-from-backpack/:b_id/:f_id");
//update
router.post("/post/backpack/:b_id"); //update a backpack
router.post("/post/update-backpack-level/:b_id");
router.get("/get/eat-normal-food-backpack/:b_id");
router.get("/get/eat-special-food-backpack/:b_id");
//delete
router.post("/post/backpack/:b_id"); //delete a backpack
router.get("/post/drop-normal-food-backpack/:b_id"); //delete a single normal food in the bag
router.get("/post/drop-special-food-backpack/:b_id"); //delete a single special food

module.exports = router;