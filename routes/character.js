const express = require("express");
const { check } = require("express-validator");

const router = express.Router();

//-------POST
//create-character
//get-character-profile=by-id
//update-character-name
//get-all-characters
//eat-meal
//change-profession
//get-all-friends
//get-friend-by-id
//update-img

module.exports = router;
