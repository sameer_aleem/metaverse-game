const express = require("express");
const router = express.Router();

/* 
u_id => user or chracter id
e_id => event-id
r_id => invite-id
*/

//create
router.post("/create/invite-request/:u_id"); //u_id is reciever
//read
router.get("/get/all-requests");
router.get("/get/request-by-id/:r_id");

router.get("/get/request-by-event-id/:e_id");

router.get("/get/all-received-request-by-userid/:u_id"); //in which im the reciver
router.get("/get/request-received-by-id/:u_id"); //in which im the reciver

router.get("/get/all-sent-request-by-userid/:u_id"); //in which im the reciver
router.get("/get/request-sent-by-id/:u_id"); //in which im the reciver

router.get("/get/all-approved-request");
router.get("/get/all-rejected-request");
router.get("/get/all-pending-request");

router.get("/get/approved-sent-request-by-userid/:u_id");
router.get("/get/rejected-sent-request-by-userid/:u_id");
router.get("/get/pending-sent-request-by-userid/:u_id");

router.get("/get/approved-received-request-by-userid/:u_id");
router.get("/get/rejected-received-request-by-userid/:u_id");
router.get("/get/pending-received-request-by-userid/:u_id");

//update
router.post("/post/accept-request/:r_id");
router.post("/post/reject-request/:r_id");

module.exports = router;