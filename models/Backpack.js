const mongoose = require("mongoose");

const backpackSchema = new mongoose.Schema({
    level: { type: Number, default: 0 }, //level of backpack
    totalFoodCount: { type: Number, default: 0 }, //all food count
    food: {
        foodType: { type: String, enum: ["Normal", "Special"] },
        foodCount: { type: Number },
        default: null
    }
}, //level decides capacity of backpack
    { timestamps: true } //keeps a track when level upgraded or backpack accessed
);

const BackPack = global.BackPack || mongoose.model("BackPack", backpackSchema);
module.exports = BackPack;
