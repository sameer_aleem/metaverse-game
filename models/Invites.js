const mongoose = require("mongoose");

const inviteSchema = new mongoose.Schema({//like push notifications
    event: { type: mongoose.Types.ObjectId, ref: "Event", required: true },
    sender: { type: mongoose.Types.ObjectId, ref: "Character", required: true },
    reciver: { type: mongoose.Types.ObjectId, ref: "Character", required: true },
    approval: {
        type: String,
        enum: ["Approved", "Pending", "Rejected"],
        default: "Pending",
        required: true
    },
},
    { timestamps: true }
);

const Invite = global.Invite || mongoose.model("Invite", inviteSchema);
module.exports = Invite;