const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const Schema = mongoose.Schema;

const eventSchema = new Schema(
  {
    name: { type: String, required: true },
    description: { type: String, required: true },
    startTime: { type: Date, required: true },
    endTime: { type: Date, required: true },
    participants: [
      {
        type: mongoose.Types.ObjectId,
        required: false,
        ref: "Character",
      },
    ],
    participationFee: { type: Number, required: true },
    participantsLimit: { type: Number, required: true },
    participantPool: { type: Number, required: false },

    status: {
      type: String,
      enum: ["yet to start", "ongoing", "ended"],
      default: "yet to start",
    },
    winners: [
      {
        type: mongoose.Types.ObjectId,
        required: false,
        ref: "Character",
      },
    ],

    reward: [{ type: Number, required: false }],
  },
  { timestamps: true }
);

//eventSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Event", eventSchema);
