const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const nftschema = new Schema(
  {
    nftType: {
      type: String,
      enum: ["art", "accessory", "outfit"],
      default: "art",
    },
    name: { type: String },
    description: { type: String },
    metaData: { type: String },
    ownerAddrss: { type: String },
    rarity: {
      type: String,
      enum: ["common", "uncommon", "rare", "epic", "legendary"],
      default: "common",
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("nft", nftschema);
