const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const Schema = mongoose.Schema;

const furnitureSchema = new Schema(
  {
    type: {
      type: String,
      enum: ["bed", "couch", "chair"],
    },

    level: {
      type: Number,
      required: true,
      default: 0,
    },

    price: { type: Number, required: false },

    owner: {
      type: mongoose.Types.ObjectId,
      required: false,
      ref: "Character",
    },
  },
  { timestamps: true }
);

//furnitureSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Furniture", furnitureSchema);
