//Author: M Abdullah Qureshi

const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const Schema = mongoose.Schema;

const schemaObject = {
  img: {
    data: Buffer,
    contentType: String,
    required: true,
  },
  name: { type: String, required: true },
  gender: {
    type: String,
    enum: ["M", "F"],
    default: "M",
    required: true,
  },
  outfit: { type: mongoose.Types.ObjectId, required: true, ref: "NFT" },
  age: {
    type: Number,
    min: [6, "User is below required age"],
    max: [100, "Unrealistic age"],
    required: true,
  },
  wallet_id: {
    type: mongoose.Types.ObjectId,
    required: true,
    unique: true,
  },
  fatigue: {
    type: Number,
    min: [0, "Fatigue cannot be below 0"],
    max: [100, "Fatigue cannot be above 100"],
    required: true,
  },
  //profession: { type: String, required: true, ref: "Job" },
  profession: {
    type: String,
    enum: ["DOCTOR", "ENGINEER", "BUILDER", "POLICEMAN"],
    default: "ENGINEER",
    required: true,
  },
  nft: [{ type: mongoose.type.ObjectId, required: true, ref: "NFT" }],
  friendsList: [
    {
      type: mongoose.type.ObjectId,
      required: false,
    },
  ],
  backpack: { type: mongoose.type.ObjectId, required: false, ref: "Backpack" },
  house: { type: mongoose.type.ObjectId, required: true, ref: "House" },
  stats: { type: mongoose.type.ObjectId, required: false, ref: "Stats" },
  invites: [{ type: mongoose.type.ObjectId, required: false, ref: "Invites" }],
};

const options = { timestamps: true };

const characterSchema = new Schema(schemaObject, options);

//characterSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Character", characterSchema);
