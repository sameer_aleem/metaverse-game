const { type } = require("express/lib/response");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const HouseSchema = new Schema(
  {
    furnituresStored: [
      {
        type: mongoose.Types.ObjectId,
      },
    ],
    nftsStored: [
      {
        type: mongoose.Types.ObjectId,
      },
    ],
    foodsStored: [
      {
        type: mongoose.Types.ObjectId,
      },
    ],
    // ownerId: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: "character",
    // },
    // houseLevel: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: "houseLevel",
    // },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model(HouseSchema, "House");
