const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const eventSchema = new Schema(
  {
    // characterID: { type: mongoose.Types.ObjectId, required: true },
    gameTimeSpend: {
      type: Number,
      default: "0",
    },
    jobTimeSpend: {
      type: String,
      default: "0",
    },
    eventAttended: [
      {
        type: String,
      },
    ],
    inGameCurrency: {
      type: Number,
      default: "0",
    },
    ownedNFTs: [
      {
        nftref: {
          type: String,
        },
      },
    ],
    lastLoginTime: {
      type: Date,
    },
    lastLogoutTime: {
      type: Date,
    },
  },
  {
    timestamps: true,
  }
);

eventSchema.plugin(uniqueValidator);

module.exports = mongoose.model("event", eventSchema);
