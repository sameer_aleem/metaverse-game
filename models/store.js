//Author: shoaib Nazeer

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const store = new Schema(
  {
    storeName: { type: String, required: true },
    furniture: [{ type: Schema.Types.ObjectId, ref: "Furniture" }],
    food: {
      foodType: { type: String, enum: ["Special", "Normal"], required: true },
      foodCount: { type: Number, required: true },
      default: null,
      required: true,
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Store", store);
