const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const jobschema = new Schema(
  {
    jobType: {
      type: String,
      enum: ["job1", "job2", "job3"],
      default: "job1",
    },
    jobDescription: { type: String },
    startTime: { type: Date },
    endTime: { type: Date },
    jobLevel: { type: Number, enum: [1, 2, 3, 4], default: 1 },
    salary: { type: Number },
    Rewards: { type: String, enum: ["Coins", "NFT"], default: "Coins" },
    employee: {
      type: mongoose.Types.ObjectId,
      required: false,
      ref: "Character",
    },

    fatigueCaused: { type: Number },
    duration: { type: Number },
  },
  { timestamps: true }
);

module.exports = mongoose.model("job", jobschema);
